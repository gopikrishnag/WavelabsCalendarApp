package com.wavelabs.service.test;

import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.any;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.calendar.repo.UserRepo;
import com.wavelabs.model.SlotInfo;
import com.wavelabs.model.User;
import com.wavelabs.model.UserType;
import com.wavelabs.service.SlotInfoService;
import com.wavelabs.service.TimeSlotService;
import com.wavelabs.service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

	@Mock
	TimeSlotService timeslotService;

	@Mock
	SlotInfoService slotinfoService;

	@InjectMocks
	private UserService userService;

	@Mock
	UserRepo userRepo;
	
	@Test
	public void testPersistProvider() {
		User provider = DataBuilder.getUser();
		when(userRepo.save(provider)).thenReturn(provider);
		User providers = userService.persistProvider(provider);
		Assert.assertEquals(provider, providers);
	}

	@Test
	public void testGetProviderById() {
		User provider = DataBuilder.getUser();
		when(userRepo.findById(anyInt())).thenReturn(provider);
		User providers = userService.getUserById(1);
		Assert.assertEquals(provider, providers);
	}

	@Test
	public void testProviderSlots() {
		List<SlotInfo> slotList = new ArrayList<SlotInfo>();
		slotList.add(DataBuilder.getSlotInfo());
		Time time = new Time(25000);
		Date date = new Date(25000);
		User provider = DataBuilder.getUser();
		when(userService.getUserById(anyInt())).thenReturn(provider);
		when(
				slotinfoService.createSlots(any(), any(), any(), any(), any(),
						anyInt())).thenReturn(slotList);
		List<SlotInfo> slots = userService.providerSlots(1, date, date,
				time, time, 1);
		Assert.assertEquals(slots, slotList);
	}

	@Test
	public void testGetAllProviders() {
		List<User> providerList = new ArrayList<User>();
		providerList.add(DataBuilder.getUser());
		when(userRepo.findByUserType(UserType.provider)).thenReturn(providerList);
		List<User> providers = userService.getAllProviders();
		Assert.assertEquals(providerList, providers);
	}
}
