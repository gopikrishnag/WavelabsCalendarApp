package com.wavelabs.service.test;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.calendar.repo.SlotInfoRepo;
import com.wavelabs.service.SlotInfoService;

@RunWith(MockitoJUnitRunner.class)
public class SlotInfoServiceTest {

	@Mock
	SlotInfoRepo slotInfoRepo;

	@InjectMocks
	private SlotInfoService slotinfoService;

	@SuppressWarnings("deprecation")
	@Test
	public void testGetListOfDates() {
		Date fromdate = new Date(2017, 8, 3);
		Date todate = new Date(2017, 8, 10);
		List<Date> datesList = slotinfoService.getListOfDates(fromdate, todate);
		Assert.assertEquals(datesList, datesList);
	}
}
