package com.wavelabs.resources.test;

import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyInt;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.model.TimeSlots;
import com.wavelabs.resources.TimeSlotResource;
import com.wavelabs.service.TimeSlotService;

@RunWith(MockitoJUnitRunner.class)
public class TimeSlotResourceTest {

	@Mock
	TimeSlotService timeslotService;

	@InjectMocks
	TimeSlotResource timeslotResource;

	@Test
	public void testGetAllTimeSlots1() {
		List<TimeSlots> timeslots = new ArrayList<TimeSlots>();
		when(timeslotService.getTimeSlotsByProvider(anyInt())).thenReturn(
				timeslots);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = timeslotResource.getAllTimeSlots(1);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}

	@Test
	public void testGetAllTimeSlots2() {
		when(timeslotService.getTimeSlotsByProvider(anyInt())).thenReturn(null);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = timeslotResource.getAllTimeSlots(1);
		Assert.assertEquals(404, entity.getStatusCodeValue());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetAllTimeSlots3() {
		when(timeslotService.getTimeSlotsByProvider(anyInt())).thenThrow(
				Exception.class);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = timeslotResource.getAllTimeSlots(1);
		Assert.assertEquals(404, entity.getStatusCodeValue());
	}
}
