package com.wavelabs.service;

import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.calendar.repo.BookingsRepo;
import com.wavelabs.calendar.repo.UserRepo;
import com.wavelabs.model.Bookings;
import com.wavelabs.model.SlotInfo;
import com.wavelabs.model.Status;
import com.wavelabs.model.TimeSlots;
import com.wavelabs.model.TokenInfo;
import com.wavelabs.model.User;
import com.wavelabs.model.UserProfile;
import com.wavelabs.model.UserType;

@Service
public class UserService {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	BookingsRepo bookingsRepo;

	@Autowired
	private TimeSlotService timeslotService;

	@Autowired
	private SlotInfoService slotinfoService;

	private Logger logger = Logger.getLogger(UserService.class);

	public User persistProvider(User user) {
		logger.info("persisting going on");
		return userRepo.save(user);
	}

	/**
	 * This method will gets the doctor based on the id
	 * 
	 * @param id
	 * @return
	 */
	public User getUserById(int id) {
		logger.info("getting provider going on");
		return userRepo.findById(id);
	}

	/**
	 * This method will persist the slots which is created by doctor
	 * 
	 * @param id
	 * @param fromdate
	 * @param todate
	 * @param fromtime
	 * @param totime
	 * @param personsPerSlot
	 * @return
	 */
	public List<SlotInfo> providerSlots(int id, Date fromdate, Date todate, Time fromtime, Time totime,
			int slotduration) {
		logger.info("creation of calendar is going on");
		User provider = getUserById(id);
		return slotinfoService.createSlots(provider, fromdate, todate, fromtime, totime, slotduration);
	}

	/**
	 * This method will gets the all the doctors in the DB
	 * 
	 * @return
	 */
	public List<User> getAllProviders() {
		return userRepo.findByUserType(UserType.provider);
	}

	/**
	 * This method will get the all the timeslots of a provider
	 * 
	 * @param id
	 * @return
	 */
	public List<TimeSlots> getTimeslotList(int id) {
		return timeslotService.getTimeSlotsByProvider(id);
	}

	/**
	 * This method will gets the Doctor based on the id
	 * 
	 * @param id
	 * @return
	 */
	public List<TimeSlots> getBookedData(int id) {
		logger.info("getting slotdata going on");
		List<TimeSlots> list = timeslotService.getTimeSlotsByProvider(id);
		return list.stream().filter(timeslot -> timeslot.getStatus() == Status.booked).collect(Collectors.toList());
	}

	/**
	 * This will gets the all the booked slot data to the doctor
	 * 
	 * @param id
	 * @return
	 */
	public String getBookedInformation(int id) {
		List<TimeSlots> timeslot = getBookedData(id);
		logger.info("getting slotdata is ready");
		String slotdata = "--------------------------------BOOKED SLOT INFORMATION--------------------------------------\n";
		for (TimeSlots slot : timeslot) {
			slotdata = slotdata + "\tTimings:  " + slot.getFromTime() + "\n";
		}
		return slotdata;
	}

	public List<Bookings> getUserHistory(int id) {
		User user=userRepo.findById(id);
		List<Bookings> bookings = bookingsRepo.findByProvider(user.getId());
		return bookings;
	}

	public String createAvailability(String id) {
		return timeslotService.createAvailability(id);
	}

	@Transactional
	public boolean updateProfile(TokenInfo tokenInfo, UserProfile userProfile) {
		boolean status = false;
		try {
			String uuid = tokenInfo.getMember().getUuid();
			String tenentId = tokenInfo.getTenantId();
			User user = userRepo.findByUuidAndTenantId(uuid, tenentId);
			user.setUserType(userProfile.getUsertype());
			// entityManager.merge(user);
			// entityManager.flush();
			userRepo.save(user);
			status = true;
		} catch (Exception e) {
			logger.info(e);
		}
		return status;
	}

	public boolean isUserExist(String uuid, String tenantId) {
		try {
			User user = userRepo.findByUuidAndTenantId(uuid, tenantId);
			return (user != null);
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}
}
