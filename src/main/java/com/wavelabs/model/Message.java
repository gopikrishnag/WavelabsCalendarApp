package com.wavelabs.model;


/**
 * This is a model class Message
 * 
 * @author tharunkumarb
 *
 */
public class Message {
	public Message() {

	}

	private int id;
	private String text;

	public int getId() {
		return id;
	}

	public void setId(int status) {
		this.id = status;
	}

	public String getText() {
		return text;
	}

	public void setText(String message) {
		this.text = message;
	}

}
