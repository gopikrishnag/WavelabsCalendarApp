package com.wavelabs.utility;

import com.wavelabs.model.Authorities;
import com.wavelabs.model.TokenInfo;

public class UserTypeChecker {

	private final static String roleName = "authority:core.module.admin";

	private UserTypeChecker() {

	}

	public static boolean isGuestUser(TokenInfo tokenInfo) {

		return (tokenInfo.getUsername() == null);

	}

	public static boolean isAdmin(TokenInfo tokenInfo) {
		Authorities[] auths = tokenInfo.getAuthorities();
		for (Authorities authority : auths) {

			if (authority.getuAuthorityName().equals(roleName)) {
				return true;
			}
		}
		return false;
	}
}
