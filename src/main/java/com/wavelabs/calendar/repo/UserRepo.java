package com.wavelabs.calendar.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.model.User;
import com.wavelabs.model.UserType;

public interface UserRepo extends CrudRepository<User, Integer> {

	public User findById(int id);

	public User findByUuidAndTenantId(String uuid, String tenantId);
	
	public List<User> findByUserType(UserType usertType);
	
}
