package com.wavelabs.calendar.repo;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.model.SlotInfo;

public interface SlotInfoRepo extends CrudRepository<SlotInfo, Integer>{

}
