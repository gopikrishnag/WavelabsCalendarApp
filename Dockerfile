FROM java:8
ADD target/calendarApp.jar calendarApp.jar
ENTRYPOINT ["java","-jar","calendarApp.jar"]
